# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL


class Template(ModelSQL, ModelView):
    _name = 'product.template'

    def __init__(self):
        super(Template, self).__init__()
        self.type = copy.copy(self.type)
        if ('value_credit', 'Value Credit') not in self.type.selection:
            self.type.selection += [('value_credit', 'Value Credit')]
        self._reset_columns()

Template()