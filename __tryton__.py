# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Contract Credit Value',
    'name_de_DE': 'Vertragsverwaltung Guthabenverwaltung',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds the management of credit values for contracts
    ''',
    'description_de_DE': '''
    - Fügt Guthabenverwaltung für Verträge hinzu.
    ''',
    'depends': [
        'account_invoice_billing_pricelist_unitprice',
        'contract_billing_pricelist_unitprice',
        'account_product_rule',
    ],
    'xml': [
        'company.xml',
        'contract.xml',
        'credit.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
