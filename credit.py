# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Bool, Not
from trytond.transaction import Transaction
from trytond.pool import Pool


class CreditValueLine(ModelSQL, ModelView):
    'Credit Value Line'
    _name = 'contract.credit.value.line'
    _description = __doc__
    _rec_name = 'contract'

    contract = fields.Many2One('contract.contract', 'Contract', required=True,
            ondelete='RESTRICT')
    amount_credit = fields.Numeric('Amount Credit',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits', 'invoice_line'], states={
                'readonly': Bool(Eval('invoice_line'))
                })
    amount_debit = fields.Numeric('Amount Debit',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits', 'invoice_line'], states={
                'readonly': Bool(Eval('invoice_line'))
                })
    invoice_line = fields.Many2One('account.invoice.line', 'Invoice Line',
            readonly=True, ondelete='CASCADE')
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
            'get_currency_digits')
    comment = fields.Char('Comment', states={
            'required': Not(Bool(Eval('invoice_line'))),
            'invisible': Bool(Eval('invoice_line'))
            }, depends=['invoice_line'])
    product = fields.Many2One('product.product', 'Product',
            domain=[('type', '=', 'value_credit')],
            ondelete='RESTRICT')
    taxes = fields.Many2Many('contract.credit.value.line-account.tax',
            'line', 'tax', 'Taxes', domain=[('parent', '=', False)])

    def __init__(self):
        super(CreditValueLine, self).__init__()
        self._error_messages.update({
            'delete_line': 'You cannot delete this value credit line!'
            })

    def default_currency_digits(self):
        company_obj = Pool().get('company.company')
        if Transaction().context.get('company'):
            company = company_obj.browse(Transaction().context['company'])
            return company.currency.digits
        return 2

    def get_currency_digits(self, ids, name):
        res = {}
        for line in self.browse(ids):
            res[line.id] = line.contract.company.currency and \
                    line.contract.company.currency.digits or 2
        return res

    def check_delete(self, ids, raise_exception=False):
        '''
        Check if the lines can be deleted
        '''
        for line in self.browse(ids):
            if not line.invoice_line or (line.invoice_line and
                    line.invoice_line.invoice and
                    line.invoice_line.invoice.state not in [
                    'draft', 'cancel']):
                if raise_exception:
                    self.raise_user_error('delete_line')
                return False
        return True

    def delete(self, ids):
        self.check_delete(ids, raise_exception=True)
        return super(CreditValueLine, self).delete(ids)

CreditValueLine()


class CreditValueLineTax(ModelSQL):
    'Credit Value Line - Tax'
    _name = 'contract.credit.value.line-account.tax'
    _description = __doc__

    line = fields.Many2One('contract.credit.value.line', 'Credit Value Line',
            ondelete='CASCADE', select=1, required=True)
    tax = fields.Many2One('account.tax', 'Tax', ondelete='RESTRICT',
            required=True)

CreditValueLineTax()
