# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval


class Company(ModelSQL, ModelView):
    _name = 'company.company'

    account_deposits_received_rule = fields.Property(fields.Many2One(
            'account.account.rule', 'Account Deposits Received Rule',
            domain=[('company', '=', Eval('active_id'))],
            depends=['active_id'],
            help='This rule will be applied for received deposits on credit values.'))
    account_deposits_paid_rule = fields.Property(fields.Many2One(
            'account.account.rule', 'Account Deposits Paid Rule',
            domain=[('company', '=', Eval('active_id'))],
            depends=['active_id'],
            help='This rule will be applied for paid deposits on credit values.'))

Company()
