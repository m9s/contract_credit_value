# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from string import Template
from trytond.model import ModelView, ModelSQL, ModelWorkflow, fields
from trytond.model import ModelSingleton
from trytond.wizard import Wizard
from trytond.pyson import Eval, Not, Equal, In
from trytond.transaction import Transaction
from trytond.pool import Pool


class Contract(ModelWorkflow, ModelSQL, ModelView):
    _name = 'contract.contract'

    credit_line = fields.Numeric('Credit Line',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits', 'state'], states={
                'readonly': In(Eval('state'), ['cancel', 'terminated'])
            })
    credit_value_lines = fields.One2Many('contract.credit.value.line',
            'contract', 'Credit Value Lines', states={
            'readonly': True
            }, depends=['invoice_method'])
    available_credit = fields.Function(fields.Numeric('Available Credit',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits'],
            help='Shows the sum of credit line and value credit balance less '
                'the sum of all draft or confirmed billing lines'),
            'get_balance')
    value_credit_balance = fields.Function(fields.Numeric('Value Credit Balance',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits'],
            help='Shows the amount of value credit left.'), 'get_balance')
    billable_amount = fields.Function(fields.Numeric('Billable Amount',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits'],
            help='Shows the amount authorized for billing.'), 'get_balance')
    is_below_warning_threshold1 = fields.Function(fields.Boolean(
            'Below Warning Threshold 1'), 'get_check_threshold')
    is_below_warning_threshold2 = fields.Function(fields.Boolean(
            'Below Warning Threshold 2'), 'get_check_threshold')

    def __init__(self):
        super(Contract, self).__init__()
        self._billing_expected_states = ['draft', 'confirmed']
        self._error_messages.update({
            'missing_payment_term': 'Missing payment term on party %s (%s)!',
            'no_account_id': 'Account rule "%s" does not have a rule for '
                'tax(es) "%s"!',
            'no_deposits_account_rule': 'No Deposits account rules defined on '
                'company %s!',
            'unbalanced_value_credit_balance': 'Contracts with an unbalanced '
                'Value Credit Balance can not be terminated!',
            })

    def get_balance(self, ids, names):
        res = {}
        if not ids:
            return res
        for name in names:
            res[name] = {}
        for contract in self.browse(ids):
            value_credit = Decimal('0.0')
            billable_amount = Decimal('0.0')
            if 'value_credit_balance' in names or 'available_credit' in names:
                for line in contract.credit_value_lines:
                    value_credit += (line.amount_debit - line.amount_credit)
                if 'value_credit_balance' in names:
                    res['value_credit_balance'][contract.id] = value_credit
            if 'billable_amount' in names or 'available_credit' in names:
                amount_to_bill = Decimal('0.0')
                for line in contract.billing_lines:
                    if line.state in self._billing_expected_states:
                        line_amount = (Decimal(str(line.quantity)) *
                            line.unit_price)
                        billable_amount += line_amount
                        if line.state == 'confirmed':
                            amount_to_bill += line_amount
                if 'billable_amount' in names:
                    res['billable_amount'][contract.id] = amount_to_bill
            if 'available_credit' in names:
                res['available_credit'][contract.id] = (value_credit * -1 +
                    contract.credit_line - billable_amount)
        return res


    def get_check_threshold(self, ids, name):
        config_obj = Pool().get('contract.configuration')
        config = config_obj.browse(config_obj.get_singleton_id())
        if name == 'is_below_warning_threshold1':
            threshold = config.warning_threshold1
        elif name == 'is_below_warning_threshold2':
            threshold = config.warning_threshold2

        res = {}
        for contract in self.browse(ids):
            res[contract.id] = False
            if self._check_threshold_condition(contract, threshold):
                res[contract.id] = True
        return res

    def _check_threshold_condition(self, contract, threshold):
        return contract.available_credit < threshold

    def bill(self, contract):
        invoice_ids = super(Contract, self).bill(contract)
        self.process_credit_value_invoices(contract, invoice_ids)
        return invoice_ids

    def process_credit_value_invoices(self, contract, invoices):
        pool = Pool()
        invoice_obj = pool.get('account.invoice')
        invoice_line_obj = pool.get('account.invoice.line')
        credit_value_line_obj = pool.get('contract.credit.value.line')
        account_rule_obj = pool.get('account.account.rule')
        tax_obj = pool.get('account.tax')

        if isinstance(contract, (int, long)):
            contract = self.browse(contract)
        if isinstance(invoices, (int, long)):
            invoices = [invoices]
        if isinstance(invoices, list):
            invoices = invoice_obj.browse(invoices)

        value_credit_balance = contract.value_credit_balance
        if not value_credit_balance < 0:
            return False

        for invoice in invoices:
            if value_credit_balance >= 0:
                break
            if abs(value_credit_balance) < abs(invoice.untaxed_amount):
                credit_value = value_credit_balance
            else:
                credit_value = invoice.untaxed_amount * -1

            credit_value_by_taxes = self._get_credit_value_by_taxes(contract)
            for key in credit_value_by_taxes.keys():
                if credit_value_by_taxes[key] >= 0:
                    continue
                elif abs(credit_value) < abs(credit_value_by_taxes[key]):
                    amount = credit_value
                else:
                    amount = credit_value_by_taxes[key]

                invoice_line_vals = {
                    'unit_price': credit_value,
                    'quantity': 1,
                    'invoice': invoice.id,
                    'taxes': [('set', list(key))]
                    }
                invoice_line_vals['description'] = (
                    self._get_invoice_line_description(contract))
                account_rule_id = self._get_credit_value_account_rule(contract)
                taxes = {'taxes': invoice_line_vals['taxes'][0][1]}
                account_pattern = account_rule_obj._get_account_rule_pattern(
                        invoice.party, {})
                account_id = account_rule_obj.apply(
                        account_rule_id, account_pattern)
                if not account_id:
                    account_rule = account_rule_obj.browse(account_rule_id)
                    taxes = ', '.join(
                        [t.name for t in tax_obj.browse(
                            invoice_line_vals['taxes'][0][1])])
                    self.raise_user_error('no_account_id', error_args=(
                        account_rule.name, taxes))
                invoice_line_vals['account'] = account_id
                invoice_line_id = self._create_invoice_line(invoice_line_vals)
                credit_value_line_vals = {
                    'amount_debit': abs(credit_value),
                    'invoice_line': invoice_line_id,
                    'contract': contract.id,
                    'taxes': [('set', list(key))]
                    }
                with Transaction().set_user(0, set_context=True):
                    credit_value_line_obj.create(credit_value_line_vals)

                credit_value -= amount
                value_credit_balance -= amount
                if credit_value <= 0:
                    break

            invoice_obj.update_taxes([invoice.id])
        return True

    def _get_invoice_line_description(self, contract):
        config_obj = Pool().get('contract.configuration')

        config = config_obj.browse(config_obj.get_singleton_id())
        mapping = self._get_mapping(contract)
        template = Template(config.description_value_credit_invoice_line or '')
        return template.substitute(mapping)

    def _get_mapping(self, contract):
        res = {
            'contract_code': contract.code or '',
            }
        return res

    def _create_invoice_line(self, line):
        invoice_line_obj = Pool().get('account.invoice.line')

        return invoice_line_obj.create(line)

    def _get_credit_value_by_taxes(self, contract):
        res = {}
        for line in contract.credit_value_lines:
            key = tuple([x.id for x in line.taxes])
            res.setdefault(key, 0)
            res[key] += (line.amount_debit - line.amount_credit)
        return res

    def _get_credit_value_account_rule(self, contract):
        if contract.invoice_type in ('out_invoice', 'out_credit_note'):
            res = contract.company.account_deposits_received_rule.id
        else:
            res = contract.company.account_deposits_paid_rule.id
        if not res:
            self.raise_user_error('no_deposits_account_rule',
                error_args=(contract.company.name,))
        return res

    def check_terminate(self, contract_id):
        # value credit balance must be balanced to *terminate* a contract
        res = super(Contract, self).check_terminate(contract_id)
        if res:
            contract = self.browse(contract_id)
            if contract.value_credit_balance != 0:
                self.raise_user_error('unbalanced_value_credit_balance')
        return res

    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['credit_value_lines'] = False
        return super(Contract, self).copy(ids, default=default)

Contract()


class Configuration(ModelSingleton, ModelSQL, ModelView):
    _name = 'contract.configuration'

    warning_threshold1 = fields.Property(fields.Numeric('Warning Threshold 1',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']))
    warning_threshold2 = fields.Property(fields.Numeric('Warning Threshold 2',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']))
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
            'get_currency_digits')
    description_value_credit_invoice_line = fields.Char(
            'Description Value Credit Invoice Line', translate=True,
            loading='lazy')

    def default_currency_digits(self):
        company_obj = Pool().get('company.company')
        if Transaction().context.get('company'):
            company = company_obj.browse(Transaction().context['company'])
            return company.currency.digits
        return 2

    def get_currency_digits(self, ids, name):
        digits = self.default_currency_digits()
        res = {}
        for record in self.browse(ids):
            res[record.id] = digits
        return res

Configuration()


class ValueCreditManagerInit(ModelView):
    'Value Credit Manager Init'
    _name = 'contract.credit.value.manager.init'
    _description = __doc__

    contract = fields.Many2One('contract.contract', 'Contract',
            readonly=True)
    contract_transfer = fields.Many2One('contract.contract',
            'Contract Transfer',
            domain=[('state', '=', 'active')],
            states={
                'required': Equal(Eval('action'), 'transfer'),
                'invisible': Not(Equal(Eval('action'), 'transfer')),
            }, depends=['action'])
    action = fields.Selection([
            ('add', 'Add credit'),
            ('transfer', 'Transfer credit'),
            ('cancel', 'Cancel credit'),
            ], 'Action', required=True, on_change=['action', 'contract'])
    amount = fields.Numeric('Amount',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits'], required=True)
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
            'get_currency_digits')
    comment = fields.Char('Comment', required=True)
    product = fields.Many2One('product.product', 'Product',
            domain=[('type', '=', 'value_credit')],
            on_change=['product', 'contract'],
            states={
                'required': Equal(Eval('action'), 'add'),
                'invisible': In(Eval('action'), ['transfer', 'cancel']),
            }, depends=['action'])
    taxes = fields.Many2Many('account.tax',
            None, None, 'Taxes', domain=[('parent', '=', False)],
            states={
                'invisible': In(Eval('action'), ['transfer', 'cancel']),
            }, depends=['action'])

    def default_action(self):
        return 'add'

    def default_currency_digits(self):
        company_obj = Pool().get('company.company')
        if Transaction().context.get('company'):
            company = company_obj.browse(Transaction().context['company'])
            return company.currency.digits
        return 2

    def get_currency_digits(self, ids, name):
        res = {}
        #for line in self.browse(ids):
        #    res[line.id] = line.contract.company.currency and \
        #            line.contract.company.currency.digits or 2
        return res

    def on_change_product(self, vals):
        pool = Pool()
        product_obj = pool.get('product.product')
        contract_obj = pool.get('contract.contract')
        tax_rule_obj = pool.get('account.tax.rule')

        res = {}
        if not vals.get('product'):
            return res
        product = product_obj.browse(vals['product'])

        party = False
        if vals.get('contract'):
            contract = contract_obj.browse(vals['contract'])
            party = contract.party_invoice

        if contract.invoice_type in ('in_invoice', 'in_credit_note'):
            res['taxes'] = []
            pattern = self._get_tax_rule_pattern(party, vals)
            for tax in product.supplier_taxes_used:
                if party and party.supplier_tax_rule:
                    tax_ids = tax_rule_obj.apply(party.supplier_tax_rule, tax,
                        pattern)
                    if tax_ids:
                        res['taxes'].extend(tax_ids)
                    continue
                res['taxes'].append(tax.id)
            if party and party.supplier_tax_rule:
                tax_ids = tax_rule_obj.apply(party.supplier_tax_rule, False,
                    pattern)
                if tax_ids:
                    res['taxes'].extend(tax_ids)
        else:
            res['taxes'] = []
            pattern = self._get_tax_rule_pattern(party, vals)
            for tax in product.customer_taxes_used:
                if party and party.customer_tax_rule:
                    tax_ids = tax_rule_obj.apply(party.customer_tax_rule, tax,
                            pattern)
                    if tax_ids:
                        res['taxes'].extend(tax_ids)
                    continue
                res['taxes'].append(tax.id)
            if party and party.customer_tax_rule:
                tax_ids = tax_rule_obj.apply(party.customer_tax_rule, False,
                        pattern)
                if tax_ids:
                    res['taxes'].extend(tax_ids)
        return res

    def _get_tax_rule_pattern(self, party, vals):
        '''
        Get tax rule pattern

        :param party: the BrowseRecord of the party
        :param vals: a dictionary with value from on_change
        :return: a dictionary to use as pattern for tax rule
        '''
        res = {}
        return res

    def on_change_action(self, vals):
        contract_obj = Pool().get('contract.contract')
        res = {}
        if vals.get('action') == 'transfer' and vals.get('contract'):
            contract = contract_obj.browse(vals['contract'])
            res['amount'] = contract.value_credit_balance * -1
        return res

ValueCreditManagerInit()


class ValueCreditManager(Wizard):
    _name = 'contract.credit.value.manager'

    states = {
        'init': {
            'actions': ['_init'],
            'result': {
                'type': 'form',
                'object': 'contract.credit.value.manager.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('choice', 'Ok', 'tryton-ok', True),
                ],
            },
        },
        'choice': {
            'result': {
                'type': 'choice',
                'next_state': '_choice',
            },
        },
        'add': {
            'result': {
                'type': 'action',
                'action': '_action_add',
                'state': 'end',
            },
        },
        'transfer': {
            'result': {
                'type': 'action',
                'action': '_action_transfer',
                'state': 'end',
            },
        },
        'cancel': {
            'result': {
                'type': 'action',
                'action': '_action_cancel',
                'state': 'end',
            },
        },
    }

    def __init__(self):
        super(ValueCreditManager, self).__init__()
        self._error_messages.update({
            'negative_balance': "You cannot transfer or cancel a higher value "
                "than the balance of the value credit.",
            'negative_value': 'The amount must be a positive value!'
            })

    def _init(self, data):
        return {'contract': data['id']}

    def _choice(self, data):
        return data['form']['action']

    def _action_add(self, data):
        credit_value_line_obj = Pool().get('contract.credit.value.line')
        # add amount to amount_credit
        # add comment
        if data['form']['amount'] < 0:
            self.raise_user_error('negative_value')
        vals = {
            'contract': data['form']['contract'],
            'amount_credit': data['form']['amount'],
            'comment': data['form']['comment'],
            'taxes': data['form']['taxes'],
            'product': data['form']['product']
        }
        with Transaction().set_user(0, set_context=True):
            credit_value_line_obj.create(vals)
        return {}

    def _action_transfer(self, data):
        credit_value_line_obj = Pool().get('contract.credit.value.line')
        contract_obj = Pool().get('contract.contract')
        contract = contract_obj.browse(data['form']['contract'])
        if data['form']['amount'] < 0:
            self.raise_user_error('negative_value')
        if abs(contract.value_credit_balance) < data['form']['amount']:
            self.raise_user_error('negative_balance')

        credit_value = data['form']['amount']
        credit_value_by_taxes = contract_obj._get_credit_value_by_taxes(
                contract)
        for key in credit_value_by_taxes.keys():
            if credit_value_by_taxes[key] >= 0:
                continue
            elif credit_value < abs(credit_value_by_taxes[key]):
                amount = credit_value
            else:
                amount = abs(credit_value_by_taxes[key])
            vals = {
                'contract': data['form']['contract'],
                'amount_debit': amount,
                'comment': data['form']['comment'],
                'taxes': [('set', list(key))],
            }
            credit_value_line_obj.create(vals)
            vals = {
                'contract': data['form']['contract_transfer'],
                'amount_credit': amount,
                'comment': data['form']['comment'],
                'taxes': [('set', list(key))],
            }
            with Transaction().set_user(0, set_context=True):
                credit_value_line_obj.create(vals)

            credit_value -= amount
            if credit_value <= 0:
                break
        return {}

    def _action_cancel(self, data):
        credit_value_line_obj = Pool().get('contract.credit.value.line')
        contract_obj = Pool().get('contract.contract')

        contract = contract_obj.browse(data['form']['contract'])

        if data['form']['amount'] < 0:
            self.raise_user_error('negative_value')
        if abs(contract.value_credit_balance) < data['form']['amount']:
            self.raise_user_error('negative_balance')

        credit_value = data['form']['amount']
        credit_value_by_taxes = contract_obj._get_credit_value_by_taxes(
                contract)
        for key in credit_value_by_taxes.keys():
            if credit_value_by_taxes[key] >= 0:
                continue
            elif credit_value < abs(credit_value_by_taxes[key]):
                amount = credit_value
            else:
                amount = abs(credit_value_by_taxes[key])
            vals = {
                'contract': data['form']['contract'],
                'amount_debit': amount,
                'comment': data['form']['comment'],
                'taxes': [('set', list(key))],
            }
            with Transaction().set_user(0, set_context=True):
                credit_value_line_obj.create(vals)

            credit_value -= amount
            if credit_value <= 0:
                break
        return {}

ValueCreditManager()
